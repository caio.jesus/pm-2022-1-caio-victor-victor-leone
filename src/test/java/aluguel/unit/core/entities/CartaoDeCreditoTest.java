package aluguel.unit.core.entities;

import org.junit.jupiter.api.Test;

import aluguel.core.entities.CartaoDeCredito;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CartaoDeCreditoTest {

    @Test
    void newCartaoDeCredito() {

        UUID id = UUID.randomUUID();
        String nomeTitular = "2";
        String numero = "3";
        Date validade = new Date();
        String cvv = "4";
        UUID ciclista = UUID.randomUUID();

        final CartaoDeCredito cartao = new CartaoDeCredito(id, nomeTitular, numero, validade, cvv, ciclista);
        
        assertEquals(cartao.getId(), id);
        assertEquals(cartao.getNomeTitular(), nomeTitular);
        assertEquals(cartao.getNumero(), numero);
        assertEquals(cartao.getValidade(), validade);
        assertEquals(cartao.getCvv(), cvv);
        assertEquals(cartao.getCiclista(), ciclista);
    }

}
