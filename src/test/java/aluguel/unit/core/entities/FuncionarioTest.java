package aluguel.unit.core.entities;

import org.junit.jupiter.api.Test;

import aluguel.core.entities.Funcionario;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FuncionarioTest {

    @Test
    void newFuncionario() {

        String matricula = UUID.randomUUID().toString();
        String senha = "2";
        String email = "3";
        String nome = "4";
        Integer idade = 5;
        String funcao = "6";
        String cpf = "7";

        final Funcionario func = new Funcionario(matricula, senha, email, nome, idade, funcao, cpf);
        
        assertEquals(func.getMatricula(), matricula);
        assertEquals(func.getSenha(), senha);
        assertEquals(func.getEmail(), email);
        assertEquals(func.getNome(), nome);
        assertEquals(func.getIdade(), idade);
        assertEquals(func.getFuncao(), funcao);
        assertEquals(func.getCpf(), cpf);
    }

}
