package aluguel.unit.core.entities;

import org.junit.jupiter.api.Test;

import aluguel.core.entities.Passaporte;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PassaporteTest {

    @Test
    void newPassaporte() {

        String numero = "1";
        Date validade = new Date();
        String pais = "2";

        final Passaporte passaporte = new Passaporte(numero, validade, pais);
        
        assertEquals(passaporte.getNumero(), numero);
        assertEquals(passaporte.getValidade(), validade);
        assertEquals(passaporte.getPais(), pais);
    }

}
