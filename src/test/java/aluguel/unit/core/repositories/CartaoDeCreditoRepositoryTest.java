package aluguel.unit.core.repositories;

import aluguel.core.entities.CartaoDeCredito;
import aluguel.core.repositories.CartaoDeCreditoRepository;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CartaoDeCreditoRepositoryTest {

    @Test
    void getByCiclistaIdTest(){
        CartaoDeCreditoRepository cdcr = new CartaoDeCreditoRepository();

        // GET BY CICLISTA ID
        Optional<CartaoDeCredito> cdc;
        cdc = cdcr.findByCiclistaId(UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897b"));
        assertTrue(cdc.isPresent());
    }

    @Test
    void updateAlreadyExistsTest(){
        CartaoDeCreditoRepository cdcr = new CartaoDeCreditoRepository();

        CartaoDeCredito cdc = new CartaoDeCredito();
        cdc.setCiclista(UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897b"));
        cdc.setCvv("987");

        cdcr.update(cdc);

        // GET BY CICLISTA ID
        Optional<CartaoDeCredito> getcdc = cdcr.findByCiclistaId(UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897b"));
        assertTrue(getcdc.isPresent());
        assertEquals("987", getcdc.get().getCvv());
    }

    @Test
    void updateNotExistsTest(){
        CartaoDeCreditoRepository cdcr = new CartaoDeCreditoRepository();

        CartaoDeCredito cdc = new CartaoDeCredito();
        cdc.setCiclista(UUID.fromString("fcf9c76a-a99f-42e7-ba0d-590857b87594"));
        cdc.setCvv("321");

        cdcr.update(cdc);

        // GET BY CICLISTA ID
        Optional<CartaoDeCredito> getcdc = cdcr.findByCiclistaId(UUID.fromString("fcf9c76a-a99f-42e7-ba0d-590857b87594"));
        assertTrue(getcdc.isPresent());
        assertEquals("321", getcdc.get().getCvv());
    }


}
