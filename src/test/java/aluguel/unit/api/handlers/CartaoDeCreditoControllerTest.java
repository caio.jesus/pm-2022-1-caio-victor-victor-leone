package aluguel.unit.api.handlers;

import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import aluguel.api.handlers.CartaoDeCreditoController;
import aluguel.core.entities.CartaoDeCredito;
import aluguel.core.repositories.CartaoDeCreditoRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.network.services.impl.ExternoService;
import io.javalin.http.Context;
import kong.unirest.UnirestException;

class CartaoDeCreditoControllerTest {

    CiclistaRepository ciclistaRepositoryMock = mock(CiclistaRepository.class);
    CartaoDeCreditoRepository cdcRepositoryMock = mock(CartaoDeCreditoRepository.class);
    ExternoService externoServiceMock = mock(ExternoService.class);
    
    Context ctx = mock(Context.class);
    
    @Test
    void getByCiclista_Success() throws JsonProcessingException, UnirestException{

        // Arrange
        CartaoDeCreditoController cdcController = new CartaoDeCreditoController(cdcRepositoryMock, ciclistaRepositoryMock, externoServiceMock);

        CartaoDeCredito cdc = new CartaoDeCredito();
        cdc.setId(UUID.randomUUID());

        doReturn(cdc.getId().toString()).when(ctx).pathParam("ciclistaId");

        when(cdcRepositoryMock.findByCiclistaId(cdc.getId())).thenReturn(Optional.of(cdc));

        // Act
        cdcController.getOneByCiclistaId(ctx);

        // Assert
        verify(ctx).status(200);
        verify(ctx).json(isNotNull());
    }

}
