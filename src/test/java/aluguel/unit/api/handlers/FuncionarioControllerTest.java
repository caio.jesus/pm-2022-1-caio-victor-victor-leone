package aluguel.unit.api.handlers;

import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import aluguel.api.handlers.FuncionarioController;
import aluguel.core.entities.Funcionario;
import aluguel.core.repositories.FuncionarioRepository;

import io.javalin.http.Context;
import kong.unirest.UnirestException;

class FuncionarioControllerTest {
    FuncionarioRepository funcionarioRepositoryMock = mock(FuncionarioRepository.class);
    
    Context ctx = mock(Context.class);
    
    @Test
    void getOne_Success() throws JsonProcessingException, UnirestException{

        // Arrange
        FuncionarioController funcionarioController = new FuncionarioController(funcionarioRepositoryMock);

        Funcionario f = new Funcionario();
        String randomMatricula = "ASDIBAFHNAOIFBALHS";
        f.setMatricula(randomMatricula);

        doReturn(f.getMatricula()).when(ctx).pathParam("funcionarioId");

        when(funcionarioRepositoryMock.findById(f.getMatricula())).thenReturn(Optional.of(f));

        // Act
        funcionarioController.getOne(ctx);

        // Assert
        verify(ctx).status(200);
        verify(ctx).json(isNotNull());
    }

}
