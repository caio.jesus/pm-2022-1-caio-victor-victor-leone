package aluguel.unit.api.handlers;

import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import aluguel.api.handlers.CiclistaController;
import aluguel.core.entities.Ciclista;
import aluguel.core.repositories.AluguelRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.network.services.IEquipamentoService;
import aluguel.network.services.IExternoService;
import io.javalin.http.Context;
import kong.unirest.UnirestException;

class CiclistaControllerTest {

    AluguelRepository aluguelRepositoryMock = mock(AluguelRepository.class);
    CiclistaRepository ciclistaRepositoryMock = mock(CiclistaRepository.class);
    IExternoService externoServiceMock = mock(IExternoService.class);
    IEquipamentoService equipamentoServiceMock = mock(IEquipamentoService.class);
    Context ctx = mock(Context.class);
    
    @Test
    void getOne_Success() throws JsonProcessingException, UnirestException{

        // Arrange
        CiclistaController CiclistaController = new CiclistaController(ciclistaRepositoryMock);

        Ciclista c = new Ciclista();
        c.setId(UUID.randomUUID());

        doReturn(c.getId().toString()).when(ctx).pathParam("ciclistaId");
        when(ciclistaRepositoryMock.findById(c.getId())).thenReturn(Optional.of(c));
        
        // Act
        CiclistaController.getOne(ctx);

        // Assert
        verify(ctx).status(200);
        verify(ctx).json(isNotNull());
    }
}
