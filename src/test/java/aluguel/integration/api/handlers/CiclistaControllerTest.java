package aluguel.integration.api.handlers;

import aluguel.api.JavalinApp;
import aluguel.core.entities.Ciclista;
import aluguel.core.entities.Passaporte;
import aluguel.core.enums.Nacionalidade;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class CiclistaControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7011;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getTests() {
        class getTest {
            public final String path;
            public final int returnCode;

            getTest(String path, int code) {
                this.path = path;
                this.returnCode = code;
            }
        }
        getTest t1 = new getTest("/ciclista", 200);
        getTest t2 = new getTest("/ciclista/0ef4982c-9569-4226-93bf-945fb4d8897b", 200);
        getTest t3 = new getTest("/ciclista/0ef4982c-9569-4226-93bf-949999999999", 404);
        getTest t4 = new getTest("/ciclista/INVALID-ID", 422);
        getTest t5 = new getTest("/ciclista/existeEmail/ciclista1@email.com", 200);
        getTest t6 = new getTest("/ciclista/existeEmail/nonexistent@email.com", 404);

        List<getTest> testList = Arrays.asList(t1, t2, t3, t4, t5, t6);
        for (getTest test : testList) {
            final HttpResponse response = Unirest.get(baseUrl + test.path).asString();
            assertEquals(test.returnCode, response.getStatus());
            assertNotNull(response.getBody());
        }

    }

    @Test
    void postCiclista_success() {
        Ciclista c = new Ciclista();
        c.setNome("test");
        c.setNacionalidade(Nacionalidade.BRASILEIRO);
        c.setEmail("email@email");
        c.setCpf("xxx.xxx.xxx.xx");

        final HttpResponse response = Unirest.post(baseUrl + "/ciclista").body(c).asString();
        assertEquals(201, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void postCiclista_fail() {
        String body = "'{\n" +
                "        \"nome\": \"ciclista2\",\n" +
                "        \"nascimento\": \"2022232-07-03\",\n" +
                "        \"cpf\": \"xxx.xxx.xxx-xx\",\n" +
                "        \"passaporte\": {\n" +
                "        \"numero\": \"xx\",\n" +
                "        \"validade\": \"2030-07-03\",\n" +
                "        \"pais\": \"BR\"\n" +
                "        },\n" +
                "        \"nacionalidade\": \"sadasd\",\n" +
                "        \"email\": \"ciclista2@email.com\"\n" +
                "        }'";

        final HttpResponse response = Unirest.post(baseUrl + "/ciclista").body(body).asString();
        assertEquals(422, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void putCiclista_success() {
        Passaporte p = new Passaporte();
        p.setNumero("xxx");
        p.setPais("BR");
        p.setValidade(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        Ciclista c = new Ciclista();
        c.setNome("ciclista1");
        c.setNacionalidade(Nacionalidade.BRASILEIRO);
        c.setEmail("ciclista1@email.com");
        c.setCpf("xxx.xxx.xxx.xx");

        final HttpResponse response = Unirest
                .put(baseUrl + "/ciclista/0ef4982c-9569-4226-93bf-945fb4d8897b")
                .body(c)
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void putCiclista_fail_nonexistent() {
        Ciclista c = new Ciclista();
        c.setNome("ciclista1");
        c.setNacionalidade(Nacionalidade.BRASILEIRO);
        c.setEmail("ciclista1@email.com");
        c.setCpf("xxx.xxx.xxx.xx");

        final HttpResponse response = Unirest
                .put(baseUrl + "/ciclista/0ef4982c-9569-4226-93bf-999999999999")
                .body(c)
                .asString();
        assertEquals(404, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void activate_success() {
        final HttpResponse response = Unirest
                .post(baseUrl + "/ciclista/6a244ffc-cbe3-44ed-bd7a-3fb91bd05369/ativar")
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void activate_fail_already_activated() {
        final HttpResponse response = Unirest
                .post(baseUrl + "/ciclista/0ef4982c-9569-4226-93bf-945fb4d8897b/ativar")
                .asString();
        assertEquals(422, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void activate_fail_not_found() {
        final HttpResponse response = Unirest
                .post(baseUrl + "/ciclista/0ef4982c-9569-4226-93bf-999999999999/ativar")
                .asString();
        assertEquals(404, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void delete_success() {
        final HttpResponse response = Unirest
                .delete(baseUrl + "/ciclista/fcf9c76a-a99f-42e7-ba0d-590857b87594")
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

}