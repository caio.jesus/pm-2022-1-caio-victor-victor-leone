package aluguel.integration.api.handlers;

import aluguel.api.JavalinApp;
import aluguel.core.entities.CartaoDeCredito;
import aluguel.core.entities.Ciclista;
import aluguel.core.enums.Status;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PostAluguel {
    String ciclista;
    String trancaInicio;

    PostAluguel (String ciclista, String trancaInicio) {
        this.ciclista = ciclista;
        this.trancaInicio = trancaInicio;
    }
}


class AluguelControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7014;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void postAluguelTest() {
        PostAluguel al = new PostAluguel("0ef4982c-9569-4226-93bf-945fb4d8897b", "00000000-0000-0000-0000-000000000001");

        final HttpResponse response = Unirest.post(baseUrl + "/aluguel").body(al).asString();
        
        assertEquals(200, response.getStatus());
        assertNotNull(response.getBody());
    }

}