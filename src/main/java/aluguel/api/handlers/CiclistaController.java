package aluguel.api.handlers;

import aluguel.api.responses.ErrorResponse;
import aluguel.core.entities.Ciclista;
import aluguel.core.enums.Status;
import aluguel.core.repositories.CiclistaRepository;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.UUID;

public class CiclistaController {

    private final CiclistaRepository ciclistas;
    private static final String CICLISTA_ID = "ciclistaId";

    public CiclistaController(CiclistaRepository ciclistas) {
        this.ciclistas = ciclistas;
    }

    public void create(@NotNull Context ctx) {
        Ciclista ciclista;
        try {
            ciclista = ctx.bodyAsClass(Ciclista.class);
        } catch (Exception exception) {
            ErrorResponse err = new ErrorResponse(null, "422", exception.getMessage());
            ctx.json(err);
            ctx.status(422);
            return;
        }

        ciclistas.save(ciclista);
        ctx.json(ciclista);
        ctx.status(201);
    }

    public void delete(@NotNull Context ctx) {
        String id = ctx.pathParam(CICLISTA_ID);
        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        ciclistas.delete(uuid);
        ctx.status(204);
    }

    public void getAll(Context ctx) {
        ctx.json(ciclistas.findAll());
    }

    public void getOne(@NotNull Context ctx) {
        String id = ctx.pathParam(CICLISTA_ID);
        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        Optional<Ciclista> ciclista = ciclistas.findById(uuid);
        handleOptionalResponse(ctx, ciclista);
    }

    private void handleOptionalResponse(Context ctx, Optional<Ciclista> ciclista) {
        if (ciclista.isPresent()) {
            ctx.json(ciclista.get());
            ctx.status(200);
            return;
        }
        ctx.status(404);
    }

    static UUID idToUUID(Context ctx, String id) {
        try{
            return UUID.fromString(id);
        } catch (IllegalArgumentException exception){
            ErrorResponse err = new ErrorResponse(id, "422", "ID is not a valid UUID");
            ctx.json(err);
            ctx.status(422);
            return null;
        }
    }

    public void update(Context ctx) {
        String id = ctx.pathParam(CICLISTA_ID);
        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        Ciclista ciclistaUpdated = ctx.bodyAsClass(Ciclista.class);
        ciclistaUpdated.setId(uuid);

        Ciclista c = tryFindCiclista(ctx, uuid);
        if (c == null) {
            return;
        }

        ciclistas.update(uuid, ciclistaUpdated);
        ctx.status(204);
    }

    public void activate(Context ctx, String id) {
        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        Ciclista ciclista = tryFindCiclista(ctx, uuid);
        if (ciclista == null) {
            return;
        }
        if (ciclista.getStatus() == Status.ATIVO) {
            ErrorResponse err = new ErrorResponse(uuid.toString(), "422", "ciclista already activated");
            ctx.json(err);
            ctx.status(422);
            return;
        }

        ciclista.setStatus(Status.ATIVO);
        ciclistas.update(uuid, ciclista);

        ctx.status(204);
    }

    private Ciclista tryFindCiclista(Context ctx, UUID uuid) {
        Optional<Ciclista> opc = ciclistas.findById(uuid);
        if (!opc.isPresent()) {
            ErrorResponse err = new ErrorResponse(uuid.toString(), "404", "ciclista not found");
            ctx.json(err);
            ctx.status(404);
            return null;
        }
        return opc.get();
    }

    public void findByEmail(Context ctx, String email) {
        Optional<Ciclista> opc = ciclistas.findByEmail(email);
        handleOptionalResponse(ctx, opc);
    }
}
