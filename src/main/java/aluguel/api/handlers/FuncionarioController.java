package aluguel.api.handlers;

import aluguel.api.responses.ErrorResponse;
import aluguel.core.entities.Funcionario;
import aluguel.core.repositories.FuncionarioRepository;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class FuncionarioController {

    private final FuncionarioRepository funcionarios;
    private static final String FUNCIONARIO_ID = "funcionarioId";

    public FuncionarioController(FuncionarioRepository funcionarios) {
        this.funcionarios = funcionarios;
    }

    public void create(@NotNull Context ctx) {
        Funcionario funcionario = funcionarioFromAPI(ctx);
        if (funcionario == null) {
            return;
        }

        funcionarios.save(funcionario);
        ctx.json(funcionario);
        ctx.status(201);
    }

    public void delete(@NotNull Context ctx) {
        String id = ctx.pathParam(FUNCIONARIO_ID);

        funcionarios.delete(id);
        ctx.status(204);
    }

    public void getAll(Context ctx) {
        ctx.json(funcionarios.findAll());
    }

    public void getOne(@NotNull Context ctx) {
        String id = ctx.pathParam(FUNCIONARIO_ID);

        Optional<Funcionario> funcionario = funcionarios.findById(id);
        handleOptionalResponse(ctx, funcionario);
    }

    private void handleOptionalResponse(Context ctx, Optional<Funcionario> funcionario) {
        if (funcionario.isPresent()) {
            ctx.json(funcionario.get());
            ctx.status(200);
            return;
        }
        ctx.status(404);
    }

    public void update(Context ctx) {
        String id = ctx.pathParam(FUNCIONARIO_ID);

        Funcionario funcionarioUpdated = funcionarioFromAPI(ctx);
        if (funcionarioUpdated == null) {
            return;
        }

        funcionarioUpdated.setMatricula(id);

        Funcionario c = tryFindFuncionario(ctx, id);
        if (c == null) {
            return;
        }

        funcionarios.update(id, funcionarioUpdated);
        ctx.status(204);
    }

    private Funcionario tryFindFuncionario(Context ctx, String matricula) {
        Optional<Funcionario> opc = funcionarios.findById(matricula);
        if (!opc.isPresent()) {
            ErrorResponse err = new ErrorResponse(matricula, "404", "funcionario not found");
            ctx.json(err);
            ctx.status(404);
            return null;
        }
        return opc.get();
    }

    private Funcionario funcionarioFromAPI(Context ctx){
        try {
            return ctx.bodyAsClass(Funcionario.class);
        } catch (Exception exception) {
            ErrorResponse err = new ErrorResponse("", "422", exception.getMessage());
            ctx.json(err);
            ctx.status(422);
            return null;
        }
    }

}
