package aluguel.api.handlers;

import aluguel.api.responses.ErrorResponse;
import aluguel.core.entities.CartaoDeCredito;
import aluguel.core.entities.Ciclista;
import aluguel.core.repositories.CartaoDeCreditoRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.network.services.impl.ExternoService;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Optional;
import java.util.UUID;

public class CartaoDeCreditoController {

    private final CartaoDeCreditoRepository cartoesDeCredito;
    private final CiclistaRepository ciclistas;
    private final ExternoService externo;
    private static final String CICLISTA_ID = "ciclistaId";

    public CartaoDeCreditoController(CartaoDeCreditoRepository cdc, CiclistaRepository cr, ExternoService ex) {
        this.cartoesDeCredito = cdc;
        this.ciclistas = cr;
        this.externo = ex;
    }

    public void getOneByCiclistaId(@NotNull Context ctx) {
        String id = ctx.pathParam(CICLISTA_ID);
        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        Optional<CartaoDeCredito> cdc = cartoesDeCredito.findByCiclistaId(uuid);
        handleOptionalResponse(ctx, cdc);
    }

    private void handleOptionalResponse(Context ctx, Optional<CartaoDeCredito> cdc) {
        if (cdc.isPresent()) {
            ctx.json(cdc.get());
            ctx.status(200);
            return;
        }
        ErrorResponse err = new ErrorResponse(null, "404", "not found");
        ctx.json(err);
        ctx.status(404);
    }

    public void update(Context ctx) throws JsonProcessingException {
        String id = ctx.pathParam(CICLISTA_ID);

        UUID uuid = idToUUID(ctx, id);
        if (uuid == null) {
            return;
        }

        Optional<Ciclista> c = this.ciclistas.findById(uuid);
        if (!c.isPresent()) {
            ErrorResponse err = new ErrorResponse(null, "404", "ciclista not found");
            ctx.json(err);
            ctx.status(404);
            return;
        }

        CartaoDeCredito cdcUpdated = cartaoDeCreditoFromAPI(ctx);
        if (cdcUpdated == null) {
            return;
        }

        boolean isCartaoValido = this.externo.validaCartaoDeCredito(cdcUpdated);
        if (!isCartaoValido){
            ErrorResponse err = new ErrorResponse(cdcUpdated.getNumero(), "422", "cartao invalido");
            ctx.json(err);
            ctx.status(422);
            return;
        }

        cdcUpdated.setCiclista(uuid);

        cartoesDeCredito.update(cdcUpdated);
        ctx.status(204);
    }


    private CartaoDeCredito cartaoDeCreditoFromAPI(Context ctx){
        try {
            return ctx.bodyAsClass(CartaoDeCredito.class);
        } catch (Exception exception) {
            ErrorResponse err = new ErrorResponse("", "422", exception.getMessage());
            ctx.json(err);
            ctx.status(422);
            return null;
        }
    }

    static UUID idToUUID(Context ctx, String id) {
        try{
            return UUID.fromString(id);
        } catch (IllegalArgumentException exception){
            ErrorResponse err = new ErrorResponse(id, "422", "ID is not a valid UUID");
            ctx.json(err);
            ctx.status(422);
            return null;
        }
    }

}
