package aluguel.api.handlers;

import aluguel.api.responses.ErrorResponse;
import aluguel.core.dto.AluguelDTO;
import aluguel.core.dto.DevolucaoDTO;
import aluguel.core.entities.Aluguel;
import aluguel.core.entities.Ciclista;
import aluguel.core.repositories.AluguelRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.CobrancaDTO;
import aluguel.network.dto.TrancaDTO;
import aluguel.network.services.IEquipamentoService;
import aluguel.network.services.IExternoService;
import io.javalin.http.Context;
import kong.unirest.UnirestException;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonProcessingException;

public class AluguelController {

    private final CiclistaRepository ciclistas;
    private final AluguelRepository alugueis;
    private final IExternoService externo;
    private final IEquipamentoService equipamento;

    private static final Double VALOR_HORA = 10.0;

    public AluguelController(AluguelRepository al, CiclistaRepository cr, IExternoService ex, IEquipamentoService eq) {
        this.alugueis = al;
        this.ciclistas = cr;
        this.externo = ex;
        this.equipamento = eq;
    }

    public void newAluguel(Context ctx) throws JsonProcessingException, UnirestException {
        AluguelDTO aluguelDTO; 
        try {
            aluguelDTO = ctx.bodyAsClass(AluguelDTO.class);
        } catch (Exception e) {
            handleError(ctx, null, 400, "wrong format for aluguel. See API reference.");
            return;
        }

        Optional<Ciclista> ciclista = this.ciclistas.findById(aluguelDTO.getCiclista());
        if (!ciclista.isPresent()) {
            handleError(ctx, aluguelDTO.getCiclista().toString(), 422, "ciclista not found.");
            return;
        }

        TrancaDTO tranca = this.equipamento.getTranca(aluguelDTO.getTrancaInicio());
        if (tranca == null) {
            handleError(ctx, aluguelDTO.getTrancaInicio().toString(), 422, "Cannot find tranca.");
            return;
        }

        BicicletaDTO bicicleta = this.equipamento.getBicicleta(tranca.getBicicleta());
        if (bicicleta == null) {
            handleError(ctx, "", 422, "Cannot find bicicleta.");
            return;
        }

        CobrancaDTO cobranca = this.externo.chargeCiclista(aluguelDTO.getCiclista(), VALOR_HORA);
        if (cobranca == null) {
            handleError(ctx, aluguelDTO.getCiclista().toString(), 422, "Cannot charge ciclista.");
            return;
        }

        Aluguel newAluguel = new Aluguel();
        newAluguel.setBicicleta(bicicleta.getId());
        newAluguel.setCiclista(aluguelDTO.getCiclista());
        newAluguel.setTrancaInicio(tranca.getId());
        newAluguel.setHoraInicio(new Date());
        newAluguel.setCobranca(cobranca.getId());
        
        this.alugueis.newAluguel(newAluguel);

        ctx.json(newAluguel);
        ctx.status(200);
    }

    public void devolucao(Context ctx) throws JsonProcessingException {
        DevolucaoDTO devolucaoDTO;
        try {
            devolucaoDTO = ctx.bodyAsClass(DevolucaoDTO.class);
        } catch (Exception e) {
            handleError(ctx, null, 400, "wrong format for devolucao. See API reference.");
            return;
        }

        Optional<Aluguel> aluguel = this.alugueis.getOngoingAluguelByBicicleta(devolucaoDTO.getBicicleta());
        if (!aluguel.isPresent()) {
            handleError(ctx, devolucaoDTO.getBicicleta().toString(), 404, "Ongoing aluguel not found. Check bicicleta ID");
            return;
        }

        Aluguel finishedAluguel = aluguel.get();
        finishedAluguel.setHoraFim(new Date());
        finishedAluguel.setTrancaFim(devolucaoDTO.getTranca());

        long extraHours = getDateDiffHours(finishedAluguel.getHoraFim(), finishedAluguel.getHoraInicio()) - 1;
        if (extraHours > 0) {
            double vlTotal = VALOR_HORA * extraHours;
            CobrancaDTO newCobranca = this.externo.chargeCiclistaWithQueue(finishedAluguel.getCiclista(), vlTotal);
            finishedAluguel.setCobranca(newCobranca.getId());

            Optional<Ciclista> c = this.ciclistas.findById(finishedAluguel.getCiclista());
            if (c.isPresent()) {
                this.externo.sendEmail(c.get().getEmail(), "Realiza cobrança extra de " + vlTotal + " por um total de " + extraHours + " horas extras");
            }
        }

        this.alugueis.updateAluguel(finishedAluguel);
        ctx.status(200);
        ctx.json(finishedAluguel);
    }

    private void handleError(Context ctx, String errId, Integer errCode, String errMessage) {
        ErrorResponse err = new ErrorResponse(errId, errCode.toString(), errMessage);
        ctx.json(err);
        ctx.status(errCode);
    }

    private long getDateDiffHours(Date date1, Date date2) {
		long diffInMillies = date1.getTime() - date2.getTime();
		return TimeUnit.MILLISECONDS.toHours(diffInMillies);
	}


}
