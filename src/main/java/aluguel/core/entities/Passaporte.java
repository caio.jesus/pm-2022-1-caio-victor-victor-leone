package aluguel.core.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Passaporte {
    private String numero;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date validade;

    private String pais;
}
