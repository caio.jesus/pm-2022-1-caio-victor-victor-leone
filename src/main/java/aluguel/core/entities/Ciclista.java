package aluguel.core.entities;

import aluguel.core.enums.Nacionalidade;
import aluguel.core.enums.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ciclista {
    private UUID id;

    private Status status;
    private String nome;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date nascimento;

    private String cpf;

    private Passaporte passaporte;
    private Nacionalidade nacionalidade;
    private String email;
}
