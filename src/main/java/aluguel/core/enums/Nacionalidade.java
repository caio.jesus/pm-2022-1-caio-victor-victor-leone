package aluguel.core.enums;

public enum Nacionalidade {
    BRASILEIRO, 
    ESTRANGEIRO;
}
