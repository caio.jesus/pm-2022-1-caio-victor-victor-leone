package aluguel.core.repositories;

import aluguel.core.entities.CartaoDeCredito;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CartaoDeCreditoRepository {
    private Map<UUID, CartaoDeCredito> cartoes;

    public CartaoDeCreditoRepository(){
        this.cartoes = new ConcurrentHashMap<>();
        CartaoDeCredito cdc1 = new CartaoDeCredito(
                UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897c"), "Victor Hugo", "5267946949340435",
                Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), "262",
                UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897b")
        );

        this.cartoes.put(cdc1.getId(), cdc1);
    }

    public Optional<CartaoDeCredito> findByCiclistaId(UUID id) {
        return this.cartoes.values().stream()
                .filter(cartao -> id.equals(cartao.getCiclista())).findFirst();
    }

    public void update(CartaoDeCredito cdc) {
        UUID uuid = cdc.getId();
        if (uuid == null) {
            Optional<CartaoDeCredito> c = this.findByCiclistaId(cdc.getCiclista());
            if (c.isPresent()) {
                uuid = c.get().getId();
            } else {
                uuid = UUID.randomUUID();
            }
        }

        CartaoDeCredito c = new CartaoDeCredito(uuid, cdc.getNomeTitular(), cdc.getNumero(), cdc.getValidade(), cdc.getCvv(), cdc.getCiclista());


        this.cartoes.put(c.getId(), c);
    }
}
