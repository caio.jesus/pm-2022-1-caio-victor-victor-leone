package aluguel.core.repositories;

import aluguel.core.entities.Ciclista;
import aluguel.core.entities.Passaporte;
import aluguel.core.enums.Nacionalidade;
import aluguel.core.enums.Status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class CiclistaRepository {
    private Map<UUID, Ciclista> ciclistas;

    public CiclistaRepository(){
        this.ciclistas = new ConcurrentHashMap<>();
        Ciclista c1 = new Ciclista(
                UUID.fromString("0ef4982c-9569-4226-93bf-945fb4d8897b"), Status.ATIVO, "ciclista1", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                "xxx.xxx.xxx-xx", new Passaporte("xx", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), "BR"),
                Nacionalidade.BRASILEIRO, "ciclista1@email.com"
        );
        Ciclista c2 = new Ciclista(
                UUID.fromString("6a244ffc-cbe3-44ed-bd7a-3fb91bd05369"), Status.AGUARDANDO_CONFIRMACAO, "ciclista2", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                "xxx.xxx.xxx-yy", new Passaporte("xx", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), "BR"),
                Nacionalidade.BRASILEIRO, "ciclista2@email.com"
        );
        Ciclista c3 = new Ciclista(
                UUID.fromString("fcf9c76a-a99f-42e7-ba0d-590857b87594"), Status.INATIVO, "ciclista3", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                "xxx.xxx.xxx-xy", new Passaporte("xx", Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), "BR"),
                Nacionalidade.BRASILEIRO, "ciclista3@email.com"
        );
        this.ciclistas.put(c1.getId(), c1);
        this.ciclistas.put(c2.getId(), c2);
        this.ciclistas.put(c3.getId(), c3);
    }

    public void save(Ciclista c) {
        c.setId(UUID.randomUUID());
        c.setStatus(Status.AGUARDANDO_CONFIRMACAO);

        this.ciclistas.put(c.getId(), c);
    }

    public Collection<Ciclista> findAll() {
        return this.ciclistas.values();
    }

    public Optional<Ciclista> findById(UUID id) {
        return Optional.ofNullable(this.ciclistas.get(id));
    }

    public Optional<Ciclista> findByEmail(String email) {
        return this.ciclistas.values().stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst();
    }

    public void update(UUID id, Ciclista c) {
        this.ciclistas.put(id, c);
    }

    public void delete(UUID id) {
        this.ciclistas.remove(id);
    }
}
