package aluguel.network.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailDTO {
    private String email;
    private String mensagem;
}
