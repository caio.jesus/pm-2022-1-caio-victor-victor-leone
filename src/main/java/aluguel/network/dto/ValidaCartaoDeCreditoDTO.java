package aluguel.network.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidaCartaoDeCreditoDTO {
    private String nomeTitular;
    private String numero;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date validade;
    
    private String cvv;
}
