package aluguel.network.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CobrarCiclistaDTO {
    private Double valor;
    private UUID ciclista;
}
