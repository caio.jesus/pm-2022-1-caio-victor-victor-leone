package aluguel.network.dto;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CobrancaDTO {
	
	private UUID ciclista;
	private Double valor;
	private UUID id;
	
	@JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss")
	private Date horaSolicitacao;
	
	@JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss")
	private Date horaFinalizacao;
	private String status;
}

