package aluguel.network.services;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;

import aluguel.core.entities.CartaoDeCredito;
import aluguel.network.dto.CobrancaDTO;

public interface IExternoService {
    public boolean validaCartaoDeCredito(CartaoDeCredito cdc) throws JsonProcessingException;
    
    public void sendEmail(String email, String mensagem) throws JsonProcessingException;

    public CobrancaDTO chargeCiclista(UUID ciclista, Double valor) throws JsonProcessingException;

    public CobrancaDTO chargeCiclistaWithQueue(UUID ciclista, Double valor) throws JsonProcessingException;
}
