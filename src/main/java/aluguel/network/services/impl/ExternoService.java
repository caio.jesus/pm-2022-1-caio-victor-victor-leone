package aluguel.network.services.impl;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import aluguel.core.entities.CartaoDeCredito;
import aluguel.network.dto.CobrancaDTO;
import aluguel.network.dto.CobrarCiclistaDTO;
import aluguel.network.dto.EmailDTO;
import aluguel.network.dto.ValidaCartaoDeCreditoDTO;
import aluguel.network.services.IExternoService;
import kong.unirest.Unirest;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;

public class ExternoService implements IExternoService {

    private ObjectMapper objectMapper = new ObjectMapper();
    private String externoUrl;

    public ExternoService(String baseUrl) {
        if ("/".equals(baseUrl.substring(baseUrl.length() - 1))) {
            this.externoUrl = baseUrl.substring(0, baseUrl.length() - 2);
        } else {
            this.externoUrl = baseUrl;
        }
    }

    public boolean validaCartaoDeCredito(CartaoDeCredito cdc) throws JsonProcessingException {

        ValidaCartaoDeCreditoDTO validarcdc = new ValidaCartaoDeCreditoDTO();
        validarcdc.setCvv(cdc.getCvv());
        validarcdc.setNomeTitular(cdc.getNomeTitular());
        validarcdc.setNumero(cdc.getNumero());
        validarcdc.setValidade(cdc.getValidade());

        String cartaoDeCreditoDTOParsed = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(validarcdc);
        HttpResponse<JsonNode> response = Unirest.post(externoUrl +"/validaCartaoDeCredito").body(cartaoDeCreditoDTOParsed).asJson();
        
        return response.isSuccess();
    }
    
    public void sendEmail(String email, String mensagem) throws JsonProcessingException {
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmail(email);
        emailDTO.setMensagem(mensagem);
        
        String enviarEmailDTOParsed = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(emailDTO);
        Unirest.post("/enviarEmail").body(enviarEmailDTOParsed);
    }

    public CobrancaDTO chargeCiclista(UUID ciclista, Double valor) throws JsonProcessingException {
        String cobrarDTOParsed = buildCobrarCiclistaDTOParsed(ciclista, valor);
        HttpResponse<JsonNode> cobrancaResponse = Unirest.post(externoUrl+"/cobranca").body(cobrarDTOParsed).asJson();

        if (!cobrancaResponse.isSuccess()) {
            return null;
        }

        return objectMapper.readValue(cobrancaResponse.getBody().getObject().toString(), CobrancaDTO.class);
    }

    public CobrancaDTO chargeCiclistaWithQueue(UUID ciclista, Double valor) throws JsonProcessingException {
        String cobrarDTOParsed = buildCobrarCiclistaDTOParsed(ciclista, valor);
        HttpResponse<JsonNode> cobrancaResponse = Unirest.post(externoUrl+"/filaCobranca").body(cobrarDTOParsed).asJson();

        if (!cobrancaResponse.isSuccess()) {
            return null;
        }

        return objectMapper.readValue(cobrancaResponse.getBody().getObject().toString(), CobrancaDTO.class);
    }

    private String buildCobrarCiclistaDTOParsed(UUID ciclista, Double valor) throws JsonProcessingException {
        CobrarCiclistaDTO cobrarCiclistaDTO = new CobrarCiclistaDTO();
        cobrarCiclistaDTO.setCiclista(ciclista);
        cobrarCiclistaDTO.setValor(valor);

        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cobrarCiclistaDTO);
    }
    
}
