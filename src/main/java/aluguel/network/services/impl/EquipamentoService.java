package aluguel.network.services.impl;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.TrancaDTO;
import aluguel.network.services.IEquipamentoService;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;

public class EquipamentoService implements IEquipamentoService {

    private ObjectMapper objectMapper = new ObjectMapper();
    private String equipamentoUrl;

    public EquipamentoService(String baseUrl) {
        if ("/".equals(baseUrl.substring(baseUrl.length() - 1))) {
            this.equipamentoUrl = baseUrl.substring(0, baseUrl.length() - 2);
        } else {
            this.equipamentoUrl = baseUrl;
        }
    }

    public TrancaDTO getTranca(UUID tranca) throws UnirestException, JsonProcessingException {
        HttpResponse<JsonNode> trancaResponse = Unirest.get(equipamentoUrl+"/tranca/"+tranca.toString()).asJson();

        if(trancaResponse.getStatus() == 404) {
			return null;
		}

        return objectMapper.readValue(trancaResponse.getBody().getObject().toString(), TrancaDTO.class);
    }

    public BicicletaDTO getBicicleta(UUID bicicleta) throws JsonProcessingException {
        if (bicicleta == null) {
            return null;
        }
        
        HttpResponse<JsonNode> bicicletaResponse = Unirest.get(equipamentoUrl+"/bicicleta/"+bicicleta.toString()).asJson();
		
		if(bicicletaResponse.getStatus() == 404) {
			return null;
		}
		
		return objectMapper.readValue(bicicletaResponse.getBody().getObject().toString(), BicicletaDTO.class);

    }
}
