package aluguel.network.services;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;

import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.TrancaDTO;
import kong.unirest.UnirestException;

public interface IEquipamentoService {

    public TrancaDTO getTranca(UUID tranca) throws UnirestException, JsonProcessingException;

    public BicicletaDTO getBicicleta(UUID bicicleta) throws JsonProcessingException;
    
}
